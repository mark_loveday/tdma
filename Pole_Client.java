/*<Applet Code="Pole_Client.class" fps=3 width=600 height=600>
</Applet>*/
/**
 * This program simulates the working of Inverted Pendulum system on client side.
 */
import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

/**
 * This class contains shared variables and methods.
 */
class Global {

public  double pos, ppos = 0, posDot, angle, angleDot, prevAngle, angleDDot, posDDot, action=0.75;
public static final double tau=0.002;
public static  ObjectOutputStream out;
public static ObjectInputStream in;

void update_pos(double pos) 	{
        synchronized(this)  { this.pos = pos; }
}
    
double get_pos()  {
        synchronized(this)  { return this.pos;}
}    

void update_ppos(double ppos) 	{
        synchronized(this)  { this.ppos = ppos; }
}
    
double get_ppos()  {
        synchronized(this)  { return this.ppos;}
}    

void update_posDot(double posDot) 	{
        synchronized(this)  { this.posDot = posDot; }
}
    
double get_posDot()  {
        synchronized(this)  { return this.posDot;}
}    

void update_posDDot(double posDDot) 	{
        synchronized(this)  { this.posDDot = posDDot; }
}
    
double get_posDDot()  {
        synchronized(this)  { return this.posDDot;}
}

void update_angle(double angle) 	{
        synchronized(this)  { this.angle = angle; }
}
    
double get_angle()  {
        synchronized(this)  { return this.angle;}
}    

void update_angleDot(double angleDot) 	{
        synchronized(this)  { this.angleDot = angleDot; }
}
    
double get_angleDot()  {
        synchronized(this)  { return this.angleDot;}
}    

void update_angleDDot(double angleDDot) 	{
        synchronized(this)  { this.angleDDot = angleDDot; }
}
    
double get_angleDDot()  {
        synchronized(this)  { return this.angleDDot;}
}    

void update_prevAngle(double prevAngle) 	{
        synchronized(this)  { this.prevAngle = prevAngle; }
}
    
double get_prevAngle()  {
        synchronized(this)  { return this.prevAngle;}
}  

void update_action(double action) 	{
        synchronized(this)  { this.action = action; }
}
    
double get_action()  {
        synchronized(this)  { return this.action;}
}  

}

/**
 * This class simulates the behaviour of Sensor. It sends angle and previous angle values to the controller to compute action.
 */
class Sensor implements Runnable {
Global data ;

Sensor(Global indata) {this.data = indata; }

void init() {
	
}

public synchronized void run() {
	while(true)
	{    
		Pole_Client.sendMessage_double(data.get_prevAngle());
		Pole_Client.sendMessage_double(data.get_angle());
		System.out.println("sent!");
		// Merge TDMA
		//Pole_Client.sendMessage_long(System.currentTimeMillis());
		try {
		   Thread.sleep(500); // sleep for 1 second
		}catch(Exception e) { e.printStackTrace(); }
	} //end while
} //end run

} //end of class

/**
 * This method simulates the behaviour of Actuator. It receives the action value from the controller and sends it across to the process.
 */
class Actuator implements Runnable {
Global data ;

Actuator(Global indata ) {this.data = indata;}

void init() {
	data.update_action(0.75);
}

public synchronized void run() {
while(true)  {
try {
	double action = data.in.readDouble();
data.update_action(action);
System.out.println("action: =================================" + action);
}
catch(Exception e)	{
	e.printStackTrace();}
	
}// end while
}//end of run()

}//end of class

/**
 * This class initializes the applet and has methods for running in a multithreaded environment.
 * It connects to the server socket and sends the pole position and in turn recieves values to apply appropriate amount of force by balance the pendulum.
 */
public class Pole_Client extends Applet implements Runnable {
	Global g = new Global();
	Socket requestSocket;
	
	String message;
	int delay;
	Thread animatorThread;
	Thread sensorThread, actuatorThread;
	/** for double-buffering
	*/
	Dimension offDimension;
	/** for double-buffering
	*/
	Image offImage;
	/** for double-buffering
	*/
	Graphics offGraphics;
	/** for pole simulation
	*/
	long startTimer, timer, stableTime, TotalTime, timer2 ;
    public static final double cartMass=1.;
    public static final double poleMass=0.1;
    public static final double poleLength=1.; ;
    public static final double forceMag= 30.;
    public static final double fricCart=0.00005;
    public static final double fricPole=0.005;
    public static final double totalMass = cartMass + poleMass;
    public static final double halfPole = 0.5 * poleLength; ;
    public static final double poleMassLength = halfPole * poleMass;
    public static final double fourthirds = 4./3.; 
	int i, j=1,k=1;
/**
 * This method initializes the pole state and sets up animation timing.
 */
  public void init() {
  		
    String str;
    int fps = 10;
	try {
	
		
	//requestSocket = new Socket("198.213.202.42", 2013);
	requestSocket = new Socket("localhost", 2013);
	g.out = new ObjectOutputStream(requestSocket.getOutputStream());
	g.out.flush();
	g.in = new ObjectInputStream(requestSocket.getInputStream()); 	
        g.update_action(0.75);
	}
	catch(IOException e) {
		System.out.println("Not able to bind to server");
	}
    // Set up animation timing.
    //How many milliseconds between frames?
    str = getParameter("fps");
    try {
      if (str != null) {
	fps = Integer.parseInt(str);
      }
    } catch (Exception e) {}
    delay = (fps > 0) ? (1000 / fps) : 100;
  }
  
/**
 * This method starts animating by creating a new Thread.
 */
  public void start() {
  
	//Start animating!
    if (animatorThread == null) {
      animatorThread = new Thread(this);
    }
    animatorThread.start();
	
	if (sensorThread == null) {
      sensorThread = new Thread(new Sensor(this.g));
    }
    sensorThread.start();
	
	if (actuatorThread == null) {
      actuatorThread = new Thread(new Actuator(this.g));
    }
    actuatorThread.start();
  }

  /**
   * This method stops the animating thread and gets rid of the objects necessary for double buffering.
   */
  public void stop() {
    //Stop the animating thread.
    animatorThread = null;
	sensorThread = null;
	actuatorThread = null;
    //Get rid of the objects necessary for double buffering.
    offGraphics = null;
    offImage = null;
	try{
				sendMessage("bye");
				g.in.close();
				g.out.close();
				requestSocket.close();
			}
			catch(IOException ioException){
				ioException.printStackTrace();
			}
  }

/**
 * This method runs the applet by first connecting to the server socket and runs the animation loop.
 * It also calculates the derivatives of the state variables and updates the state of the pole.
 * It sends across the position values, updates them and the value of action.
 */
  public void run() {
    
    //Remember the starting time.
    long startTime = System.currentTimeMillis();
	startTimer = System.currentTimeMillis();
	timer = System.currentTimeMillis();
	timer2 = System.currentTimeMillis();		
			
    //This is the animation loop.
    while (Thread.currentThread() == animatorThread) {
	
	  //Update the state of the pole; 
      // First calc derivatives of state variables
      double force = forceMag * g.get_action();
      double sinangle = Math.sin(g.get_angle()); 
      double cosangle = Math.cos(g.get_angle());
      double angleDotSq = g.get_angleDot() * g.get_angleDot();
      double common = (force + poleMassLength * angleDotSq * sinangle
               - fricCart * (g.get_posDot()<0 ? -1 : 0)) / totalMass;
      g.update_angleDDot ((9.8* sinangle - cosangle * common
			  - fricPole * g.get_angleDot() / poleMassLength) /
	(halfPole * (fourthirds - poleMass * cosangle * cosangle /
		     totalMass)));
      g.update_posDDot(common - poleMassLength * g.get_angleDDot() * cosangle /
	totalMass);
	
	  //Now update state.	  
      System.out.println("angle = " + g.get_angle());
	  System.out.println("action = " + g.get_action());
	  System.out.println("pos" + g.get_pos());
	  System.out.println("posDot" + g.get_posDot());
	  System.out.println("angleDot" + g.get_angleDot());
	  
          
          { // update status
			double x=0.;
			x=g.get_pos();
			x+=g.get_posDot() * g.tau;
			g.update_pos(x);

			x=g.get_posDot();
			x+=g.get_posDDot() * g.tau;
			g.update_posDot(x);

			g.update_prevAngle(g.get_angle());

			x=g.get_angle();
			x+=g.get_angleDot() * g.tau;
			g.update_angle(x);

			x=g.get_angleDot();
			x+=g.get_angleDDot() * g.tau;
			g.update_angleDot(x);
          }
          
          
          try
			{
				Thread.sleep(10);
			}catch(Exception e)
			{
				e.printStackTrace();
			}
          
          
	  try{
	 
      // If current angle is within (-1, 1) for 10 seconds, it is considered to be the success!
	  if ( (g.get_angle()*180/3.14) > -1.0 && (g.get_angle()*180/3.14) < 1.0) 
	  { 
		if ( (System.currentTimeMillis() - timer) > 10000 ) {  
			resetPole();
			g.update_action(0);
		
			if(j==1)
			{
				stableTime = System.currentTimeMillis() - timer;
				j=0;
			}
		
			if(k==1)
			{
				TotalTime = System.currentTimeMillis() - timer2;
				k=0;
			}
		
		}		
	  }
	  else
		timer = System.currentTimeMillis();
      }

	catch(Exception e)	{
	e.printStackTrace();}		
 
    //Display it.
    repaint();

    //Delay depending on how far we are behind.
    try {
	startTime += delay;
	Thread.sleep(Math.max(0, 
			      startTime-System.currentTimeMillis()));
      } catch (InterruptedException e) {
	break;
      }
    }
		sendMessage("bye");
	 
  }
/**
 * This method sends the String message on the object output stream.
 */
  static void sendMessage(String msg)
	{
		try{
			Global.out.writeObject(msg);
			Global.out.flush();
			System.out.println("client>" + msg);
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
/**
 * This method sends the Double message on the object output stream.
 */  
  static void sendMessage_double(double msg)
	{
		try{
			Global.out.writeDouble(msg);
			Global.out.flush();
			System.out.println("client>" + msg);
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
  
  // Added send long to send timestamp for merging TDMA
  static void sendMessage_long(long msg){
	  try{
		  Global.out.writeLong(msg);
		  Global.out.flush();
		  System.out.println("client>" + msg);
	  }
	  catch(IOException ioException){
		  ioException.printStackTrace();
	  }
  }

/**
 * This method paints the graphics by calling the update method.
 */  
  public void paint(Graphics gr) {
    update(gr);
  }
/**
 * This method draws the track, pole, cart, action arrow and also erases previous image.
 */  
  public void update(Graphics gr) {
    Color bg = getBackground();
    Color fg = getForeground();
    Dimension d = getSize();
    Color cartColor = new Color(255,69,0);
    Color arrowColor = new Color(255,255,0);
    Color trackColor = new Color(100,100,50);

    //Create the offscreen graphics context, if no good one exists.
    if ( (offGraphics == null)
	 || (d.width != offDimension.width)
	 || (d.height != offDimension.height) ) {
      offDimension = d;
      offImage = createImage(d.width, d.height);
      offGraphics = offImage.getGraphics();
    }

    //Erase the previous image.
    offGraphics.setColor(getBackground());
    offGraphics.fillRect(0,0,d.width,d.height);

    //Draw Track.
    double xs[] = {-2.5, 2.5, 2.5, 2.3, 2.3, -2.3, -2.3, -2.5};
    double ys[] = {-0.4, -0.4, 0., 0., -0.2, -0.2, 0, 0};
    int pixxs[] = new int[8], pixys[] = new int[8];
    for (int i = 0; i<8; i++) {
      pixxs[i] = pixX(d,xs[i]);
      pixys[i] = pixY(d,ys[i]);
      }
    offGraphics.setColor(trackColor);
    offGraphics.fillPolygon(pixxs,pixys,8);
	
	//Draw message
    String msg = "CS386C DCS: Inverted Pendulum Project";
    offGraphics.drawString(msg,20,d.height-20);
	
	msg = "Angle in Degrees = " + g.get_angle()*180/3.14;
    offGraphics.drawString(msg,20,d.height-40);
	
	msg = "Timer = " + ((System.currentTimeMillis()-startTimer)/1000) + " secs";
    offGraphics.drawString(msg,20,d.height-60);
	
	if(k==0) {
	msg = "Total Time To Reach Stable State = " + TotalTime/1000 + " secs";
    offGraphics.drawString(msg,20,d.height-80);
	}
	
	g.update_ppos(g.get_pos() %(2.5));
	
    //Draw cart.
    offGraphics.setColor(cartColor);
    offGraphics.fillRect(pixX(d,g.get_ppos()-0.2), pixY(d,0), pixDX(d,0.4), pixDY(d,-0.2));

    //Draw pole.
    //    offGraphics.setColor(cartColor);
    offGraphics.drawLine(pixX(d,g.get_ppos()),pixY(d,0),
			 pixX(d,g.get_ppos()+Math.sin(g.get_angle())*poleLength),
			 pixY(d,poleLength*Math.cos(g.get_angle())));

    //Draw action arrow.
    if (g.get_action() != 0) {
      int signAction = (g.get_action() > 0 ? 1 : (g.get_action() < 0) ? -1 : 0);
      int tipx = pixX(d,g.get_ppos()+0.2*signAction);
      int tipy = pixY(d,-0.1);
      offGraphics.setColor(arrowColor);
      offGraphics.drawLine(pixX(d,g.get_ppos()),pixY(d,-0.1),tipx,tipy);
      offGraphics.drawLine(tipx,tipy,tipx-4*signAction,tipy+4);
      offGraphics.drawLine(tipx,tipy,tipx-4*signAction,tipy-4);
    }
    
    //Last thing: Paint the image onto the screen.
    gr.drawImage(offImage, 0, 0, this);

  }

  public int pixX(Dimension d, double v) {
  
    return (int) Math.round((v + 2.5) / 5.0 * d.width);
  }

  public int pixY(Dimension d, double v) {
    return (int) Math.round(d.height - (v + 2.5) / 5.0 * d.height);
  }

  public int pixDX(Dimension d, double v) {
    return (int) Math.round(v / 5.0 * d.width);
  }

  public int pixDY(Dimension d, double v) {
    return (int) Math.round(-v / 5.0 * d.height);
  }

 /**
 * This method resets the pole position values.
 */  
  public void resetPole() {
    g.update_pos(0.);
    g.update_posDot(0.);
    g.update_angle(0.);
    g.update_angleDot(0.);
  }
}
