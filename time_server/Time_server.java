//testing comments

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package time_server;

import java.io.*;
import java.net.*;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;



class BroadcastTimer extends java.util.TimerTask{
      ObjectOutputStream out;

      BroadcastTimer(ObjectOutputStream out)
      {
          this.out = out;
      
      }
      
        @Override
        public void run() {
            try
            {
                // sent current sever time to the client
                out.writeLong(System.currentTimeMillis());
                out.flush();
            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }
/**
 *
 * @author xmzhu
 */
public class Time_server extends Thread  {

    ServerSocket providerSocket;
	Socket connection = null;
	ObjectOutputStream out;
	ObjectInputStream in;
	String message;
        
        final long client_period = 2000; //2 seconds
        
        Timer timer;        
        long Last_arrival_time = 0;
        
        
        Time_server(Socket socket){
        	connection = socket;
        }
       
        
        // This is called when each 
        @Override
        public void run(){
        	Wait_connection();
        	timer = new Timer();
        	timer.schedule(new BroadcastTimer(out),1000,10000);
            echo();
        }
 
        
       void Wait_connection()
        {
            try{
                

                 //providerSocket = new ServerSocket(1000, 10);
                 
	                 //connection = providerSocket.accept();
	                 System.out.println("connected");
	                 out = new ObjectOutputStream(connection.getOutputStream());
	                 out.flush();
	                 in = new ObjectInputStream(connection.getInputStream());
                 
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        
        void Clear()
        {
            try
            {
            this.connection.close();
            
            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        
         void echo()
        {System.out.println("inside echo");
            try
            {
                while (true)
                {
                	System.out.println("inside while");
                   long ctime = in.readLong();
                   long timestamp = System.currentTimeMillis();
                   if (this.Last_arrival_time != 0)
                   {
                       // If your synchronization mechanism is corrent
                       // it may print out "Interval Differences : 0". The number is small
                       // If you implementationis wrong, sometimes, it will print out big number like
                       // "Interval Differences : 200" 
                       System.out.println("Client Interval Differences:" +(timestamp-this.Last_arrival_time - this.client_period));
                   }
                   
                   this.Last_arrival_time = timestamp;
                   System.out.println(this.Last_arrival_time);
                   /* 
                   String offset_file = "F:\\offset.txt";
                   DataOutputStream  dos = new DataOutputStream(new FileOutputStream(offset_file,
                                            true));
                   dos.writeLong(offset);
                   dos.close();
                   */
              
                  // this.out.writeLong(-n);
                 //  this.out.flush();
                   
                 //  System.out.println(timestamp+": write "+(-n));
                   
                }
                
            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        
   
    /**
     * @param args the command line arguments
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
    	//Time_server server = new Time_server();
        ServerSocket providerSocket = new ServerSocket(1000,10);
        
        // Loop to wait for clients to connect to the server
        while(true)
        {
        	Socket client = providerSocket.accept();
        	Time_server ts = new Time_server(client);
        	
        	ts.start();
            //server.Wait_connection();
            //server.Get_Timeoffset();
            //server.echo();
        }
        //server.run();
        
        //server.timer = new Timer();
        
        // send system timer every 10 seconds
        //server.timer.schedule(new BroadcastTimer(server.out),1000,10000);
        
        //server.echo();
        
       
    }



}
