/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualmedia;



import java.io.*;
import java.net.*;
import java.util.*;




class Uplink_Thread extends Thread
{
	public ObjectOutputStream vmToServer;
	public ObjectInputStream clientToVm;

	FailurePattern fps;
	double lossr;

	Random r;
	public Uplink_Thread(OutputStream o, InputStream s, FailurePattern fp, double lr) throws IOException
	{
		clientToVm = new ObjectInputStream(s);
		vmToServer = new ObjectOutputStream(o);
		vmToServer.flush();
		fps = fp;
		lossr = lr;
		r = new Random();
	}

	public Uplink_Thread() {
		// TODO Auto-generated constructor stub
	}

	public static void sendMessage_double(double msg, ObjectOutputStream os)
		{
			try{
				os.writeDouble(msg);
				os.flush();
				System.out.println("client>" + msg);
			}
			catch(IOException ioException){
				ioException.printStackTrace();
			}
		}
    public void run()
    {
 		try{
 			double val;
 			int locked = 0;
 			while(true){
 				if(fps == FailurePattern.Random_Loss)
 				{
 					double rv = r.nextDouble();
 					double v1 = clientToVm.readDouble();
 					double v2 = clientToVm.readDouble();
 					if(rv >= lossr)
 					{
		 				sendMessage_double(v1, vmToServer);
		 				sendMessage_double(v2, vmToServer);
 					}
 				}
 				else if(fps == FailurePattern.Cluster_Loss)
 				{
 					double rv = r.nextDouble();
 					double v1 = clientToVm.readDouble();
 					double v2 = clientToVm.readDouble();
 					if(locked <= 0 && rv < lossr)
 					{
 						locked = 10;
 					}
 					if(locked <= 0)
 					{
		 				sendMessage_double(v1, vmToServer);
		 				sendMessage_double(v2, vmToServer);
 					}
 					else
 					{
 						locked--;
 					}
 					
 				}
 				else
 				{
 					double v1 = clientToVm.readDouble();
 					double v2 = clientToVm.readDouble();
	 				sendMessage_double(v1, vmToServer);
	 				sendMessage_double(v2, vmToServer);
 				}
 				//Thread.sleep(1000);
 			}
 			
 		}catch (IOException e)
    	{
    		System.out.println("not accepted");
    	}
 		
    }
}

class Downlink_Thread extends Thread
{    
	public ObjectOutputStream vmToClient;
	public ObjectInputStream serverToVm;
	
	FailurePattern fps;
	double lossr;
	Random r;
	public Downlink_Thread(OutputStream o, InputStream s, FailurePattern fp, double lr) throws IOException
	{
		vmToClient = new ObjectOutputStream(o);
		vmToClient.flush();
		serverToVm = new ObjectInputStream(s);
		r = new Random();
		fps = fp;
		lossr = lr;
	}
	public static void sendMessage_double(double msg, ObjectOutputStream os)
	{
		try{
			os.writeDouble(msg);
			os.flush();
			System.out.println("server>" + msg);
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
    public Downlink_Thread() {
		// TODO Auto-generated constructor stub
	}

	public void run()
    {
 		try{
 			double val;
 			while(true){
 				sendMessage_double(serverToVm.readDouble(), vmToClient);
 				//Thread.sleep(2000);
 			}
 			
 		}catch (IOException e)
    	{
    		System.out.println("not accepted");
    	}
    }
}


 enum FailurePattern
{
     Random_Loss, // Communication is affected by random noise
     Cluster_Loss,  // Communication is affected by persistent noise for a time
     None,
}
/**
 * VirtualMedia
 *  The program serves as a mid-man between the physical process and the controller
 * @author xmzhu
 */
public class VirtualMedia {
    
    Uplink_Thread uplink;  // relay client packets to server
    Downlink_Thread downlink; // relay server packets to client
    
    FailurePattern pattern;
    
    double lossratio;
	private static Socket serverSocket;
	private static ServerSocket clientSocket;
	private static final int port =2013;
     // connect the server and the client
	
	
     void Connect(String errorType, double errorRatio) throws IOException
     {

 		try{
 			lossratio = errorRatio;
 			if(errorType.equals("none"))
 				pattern = FailurePattern.None;
 			else if(errorType.equals("random"))
 				pattern = FailurePattern.Random_Loss;
 			else // "cluster"
 				pattern = FailurePattern.Cluster_Loss;
 			serverSocket = new Socket("localhost", 2012);
 			clientSocket = new ServerSocket(port);
 			Socket s = clientSocket.accept();
 			
 	         // Connect Client
 			uplink = new Uplink_Thread(serverSocket.getOutputStream(), s.getInputStream(), pattern, lossratio);
 			
 	          // Connect Server
 			downlink = new Downlink_Thread(s.getOutputStream(), serverSocket.getInputStream(), pattern, lossratio);
 			
 		}catch (IOException ioe){
 			System.out.println("unable to set up port");
 			System.exit(1);
 		}
 		System.out.println("Waiting for connection");
          

     }
     
     
     
     

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        String errorType = args[0];
        double errorRatio = Double.parseDouble(args[1]);
    	
        VirtualMedia vm = new VirtualMedia();
        vm.Connect(errorType, errorRatio);
        
        
        vm.uplink.start();
        vm.downlink.start();
    }
}
